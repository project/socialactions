<?php
class socialactions_port
{
	
	const SOCIAL_ACTION_URL = 'http://search.socialactions.com/actions.json';
	
	static public function get_response( socialactions_request $objRequest )
	{
		$objCurlRequest = curl_init( self::get_request_url( $objRequest ) );
		curl_setopt( $objCurlRequest, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $objCurlRequest, CURLOPT_HEADER, false );
		$objResult = curl_exec( $objCurlRequest );
		curl_close( $objCurlRequest );
		
		return  json_decode( utf8_encode( $objResult ) );
	}
	
	static public function get_request_url( socialactions_request $objRequest )
	{
		$strRequestUrl = socialactions_port::SOCIAL_ACTION_URL;
		$strRequestUrlPartsArray = array();
		
		if( count( $objRequest->getWordsArray() ) > 0 )
		{
			$strRequestUrlPartsArray[] = sprintf( 'q=%s', implode( '+', $objRequest->getWordsArray() ) );
		}
		
		if( count( $objRequest->getActionTypesArray() ) > 0 )
		{
			$strRequestUrlPartsArray[] = sprintf( 'action_types=%s', implode( ',', $objRequest->getActionTypesArray() ) );
		}
		
		$strRequestUrlPartsArray[] = sprintf( 'created=%s', $objRequest->getCreated() );
		$strRequestUrlPartsArray[] = sprintf( 'match=%s', $objRequest->getMatch() );
		$strRequestUrlPartsArray[] = sprintf( 'limit=%d', $objRequest->getLimit() );
		$strRequestUrlPartsArray[] = sprintf( 'order=%s', $objRequest->getOrderBy() );
		
		$strRequestUrl = sprintf( '%s?%s', 
									socialactions_port::SOCIAL_ACTION_URL,
									implode( '&', $strRequestUrlPartsArray ) );
									
		return $strRequestUrl;
	}
	
}